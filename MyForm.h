#pragma once

namespace Tarea2JoshuaValey1014416 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Diagnostics;


	/// <summary>
	/// Resumen de MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		MyForm(void)
		{
			InitializeComponent();
			//
			//TODO: agregar c�digo de constructor aqu�
			//
		}

	protected:
		/// <summary>
		/// Limpiar los recursos que se est�n usando.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::TabControl^ tabControl1;
	protected:
	private: System::Windows::Forms::TabPage^ tabDivision;



	private: System::Windows::Forms::Label^ label1;
	private: System::Windows::Forms::TextBox^ txtNum1;
	private: System::Windows::Forms::TextBox^ txtNum2;



	private: System::Windows::Forms::Button^ btnOperar;


	private: System::Windows::Forms::Label^ lblResultadoDivision;

	private: System::Windows::Forms::Label^ label3;
	private: System::Windows::Forms::Label^ label2;
	private: System::Windows::Forms::Button^ btnMcd;
	private: System::Windows::Forms::Button^ btnTriangulo;
	private: System::Windows::Forms::TextBox^ txtGradoBinomio;
	private: System::Windows::Forms::Label^ label4;
	private: System::Windows::Forms::Label^ lblTriangulo;
	private: System::Windows::Forms::Label^ label5;
	private: System::Windows::Forms::Button^ btnTrianguloIterativo;

	private:
		/// <summary>
		/// Variable del dise�ador necesaria.
		/// </summary>
		System::ComponentModel::Container^ components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// M�todo necesario para admitir el Dise�ador. No se puede modificar
		/// el contenido de este m�todo con el editor de c�digo.
		/// </summary>
		void InitializeComponent(void)
		{
			this->tabControl1 = (gcnew System::Windows::Forms::TabControl());
			this->tabDivision = (gcnew System::Windows::Forms::TabPage());
			this->btnTriangulo = (gcnew System::Windows::Forms::Button());
			this->txtGradoBinomio = (gcnew System::Windows::Forms::TextBox());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->lblTriangulo = (gcnew System::Windows::Forms::Label());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->btnMcd = (gcnew System::Windows::Forms::Button());
			this->btnOperar = (gcnew System::Windows::Forms::Button());
			this->lblResultadoDivision = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->txtNum1 = (gcnew System::Windows::Forms::TextBox());
			this->txtNum2 = (gcnew System::Windows::Forms::TextBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->btnTrianguloIterativo = (gcnew System::Windows::Forms::Button());
			this->tabControl1->SuspendLayout();
			this->tabDivision->SuspendLayout();
			this->SuspendLayout();
			// 
			// tabControl1
			// 
			this->tabControl1->Controls->Add(this->tabDivision);
			this->tabControl1->Location = System::Drawing::Point(1, 1);
			this->tabControl1->Name = L"tabControl1";
			this->tabControl1->SelectedIndex = 0;
			this->tabControl1->Size = System::Drawing::Size(677, 526);
			this->tabControl1->TabIndex = 0;
			// 
			// tabDivision
			// 
			this->tabDivision->Controls->Add(this->btnTrianguloIterativo);
			this->tabDivision->Controls->Add(this->btnTriangulo);
			this->tabDivision->Controls->Add(this->txtGradoBinomio);
			this->tabDivision->Controls->Add(this->label4);
			this->tabDivision->Controls->Add(this->lblTriangulo);
			this->tabDivision->Controls->Add(this->label5);
			this->tabDivision->Controls->Add(this->btnMcd);
			this->tabDivision->Controls->Add(this->btnOperar);
			this->tabDivision->Controls->Add(this->lblResultadoDivision);
			this->tabDivision->Controls->Add(this->label3);
			this->tabDivision->Controls->Add(this->label2);
			this->tabDivision->Controls->Add(this->txtNum1);
			this->tabDivision->Controls->Add(this->txtNum2);
			this->tabDivision->Controls->Add(this->label1);
			this->tabDivision->Location = System::Drawing::Point(4, 22);
			this->tabDivision->Name = L"tabDivision";
			this->tabDivision->Padding = System::Windows::Forms::Padding(3);
			this->tabDivision->Size = System::Drawing::Size(669, 500);
			this->tabDivision->TabIndex = 0;
			this->tabDivision->Text = L"Divisi�n Por Restas";
			this->tabDivision->UseVisualStyleBackColor = true;
			// 
			// btnTriangulo
			// 
			this->btnTriangulo->Location = System::Drawing::Point(421, 270);
			this->btnTriangulo->Name = L"btnTriangulo";
			this->btnTriangulo->Size = System::Drawing::Size(240, 23);
			this->btnTriangulo->TabIndex = 13;
			this->btnTriangulo->Text = L"Generar Triangulo Recursivo";
			this->btnTriangulo->UseVisualStyleBackColor = true;
			this->btnTriangulo->Click += gcnew System::EventHandler(this, &MyForm::BtnTriangulo_Click);
			// 
			// txtGradoBinomio
			// 
			this->txtGradoBinomio->Location = System::Drawing::Point(531, 236);
			this->txtGradoBinomio->Name = L"txtGradoBinomio";
			this->txtGradoBinomio->Size = System::Drawing::Size(100, 20);
			this->txtGradoBinomio->TabIndex = 12;
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(434, 239);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(91, 13);
			this->label4->TabIndex = 11;
			this->label4->Text = L"Grado de Binomio";
			// 
			// lblTriangulo
			// 
			this->lblTriangulo->AutoSize = true;
			this->lblTriangulo->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
										static_cast<System::Byte>(0)));
			this->lblTriangulo->Location = System::Drawing::Point(22, 277);
			this->lblTriangulo->Name = L"lblTriangulo";
			this->lblTriangulo->Size = System::Drawing::Size(221, 16);
			this->lblTriangulo->TabIndex = 10;
			this->lblTriangulo->Text = L"Resultado del Triangulo aqui...";
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
								  static_cast<System::Byte>(0)));
			this->label5->Location = System::Drawing::Point(17, 236);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(261, 25);
			this->label5->TabIndex = 9;
			this->label5->Text = L"Ejercicio Triangulo de pascal";
			// 
			// btnMcd
			// 
			this->btnMcd->Location = System::Drawing::Point(271, 136);
			this->btnMcd->Name = L"btnMcd";
			this->btnMcd->Size = System::Drawing::Size(75, 23);
			this->btnMcd->TabIndex = 7;
			this->btnMcd->Text = L"MCD";
			this->btnMcd->UseVisualStyleBackColor = true;
			this->btnMcd->Click += gcnew System::EventHandler(this, &MyForm::BtnMcd_Click);
			// 
			// btnOperar
			// 
			this->btnOperar->Location = System::Drawing::Point(154, 136);
			this->btnOperar->Name = L"btnOperar";
			this->btnOperar->Size = System::Drawing::Size(75, 23);
			this->btnOperar->TabIndex = 6;
			this->btnOperar->Text = L"Division";
			this->btnOperar->UseVisualStyleBackColor = true;
			this->btnOperar->Click += gcnew System::EventHandler(this, &MyForm::Button1_Click);
			// 
			// lblResultadoDivision
			// 
			this->lblResultadoDivision->AutoSize = true;
			this->lblResultadoDivision->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
												static_cast<System::Byte>(0)));
			this->lblResultadoDivision->ForeColor = System::Drawing::Color::Red;
			this->lblResultadoDivision->Location = System::Drawing::Point(17, 188);
			this->lblResultadoDivision->Name = L"lblResultadoDivision";
			this->lblResultadoDivision->Size = System::Drawing::Size(240, 25);
			this->lblResultadoDivision->TabIndex = 5;
			this->lblResultadoDivision->Text = L"Esperando Operaci�n...";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(54, 104);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(90, 13);
			this->label3->TabIndex = 4;
			this->label3->Text = L"Numero 1/Divisor";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(340, 104);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(106, 13);
			this->label2->TabIndex = 3;
			this->label2->Text = L"Numero 2/Dividendo";
			this->label2->Click += gcnew System::EventHandler(this, &MyForm::Label2_Click);
			// 
			// txtNum1
			// 
			this->txtNum1->Location = System::Drawing::Point(57, 67);
			this->txtNum1->Name = L"txtNum1";
			this->txtNum1->Size = System::Drawing::Size(100, 20);
			this->txtNum1->TabIndex = 2;
			// 
			// txtNum2
			// 
			this->txtNum2->Location = System::Drawing::Point(346, 67);
			this->txtNum2->Name = L"txtNum2";
			this->txtNum2->Size = System::Drawing::Size(100, 20);
			this->txtNum2->TabIndex = 1;
			this->txtNum2->TextChanged += gcnew System::EventHandler(this, &MyForm::TextBox1_TextChanged);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 15, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
								  static_cast<System::Byte>(0)));
			this->label1->Location = System::Drawing::Point(17, 16);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(460, 25);
			this->label1->TabIndex = 0;
			this->label1->Text = L"Ejercicio Divisi�n de dos n�meros por restas y MCD\r\n";
			// 
			// btnTrianguloIterativo
			// 
			this->btnTrianguloIterativo->Location = System::Drawing::Point(421, 299);
			this->btnTrianguloIterativo->Name = L"btnTrianguloIterativo";
			this->btnTrianguloIterativo->Size = System::Drawing::Size(237, 23);
			this->btnTrianguloIterativo->TabIndex = 14;
			this->btnTrianguloIterativo->Text = L"Generar Triangulo Iterativo";
			this->btnTrianguloIterativo->UseVisualStyleBackColor = true;
			this->btnTrianguloIterativo->Click += gcnew System::EventHandler(this, &MyForm::BtnTrianguloIterativo_Click);
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(678, 523);
			this->Controls->Add(this->tabControl1);
			this->Name = L"MyForm";
			this->Text = L"MyForm";
			this->tabControl1->ResumeLayout(false);
			this->tabDivision->ResumeLayout(false);
			this->tabDivision->PerformLayout();
			this->ResumeLayout(false);

		}
#pragma endregion

#pragma region metodos Ejercicio 1

		/*Stopwatch^ tiempo_ejecucion;
		Int64 tiempo_division_resta;
		Int64 tiempo_division_residuo;
		Int64 tiempo_division_residuo_iterativo;*/
		

		int DivisionResta(int divisor, int dividendo)
		{	
			//tiempo_ejecucion = gcnew Stopwatch->StartNew();
			
			int resultado;
			if (divisor < dividendo)
				resultado = 0;
			else
				resultado = DivisionResta((divisor - dividendo), dividendo) + 1;

			//tiempo_ejecucion->Stop();
			//tiempo_division_resta += tiempo_ejecucion->ElapsedTicks;
			return resultado;
		}

		int ResiduoResta(int div, int divd)
		{
			//tiempo_ejecucion = gcnew Stopwatch->StartNew();
			if (div < divd)
				return div;
			else 
			{
				//tiempo_ejecucion->Stop();
				//tiempo_division_residuo += tiempo_ejecucion->ElapsedTicks;
				return ResiduoResta((div - divd), divd);
			}
			
		}

		String^ DivisionResiduoIterativo(int divisor, int dividendo)
		{
			//tiempo_ejecucion = gcnew Stopwatch->StartNew();
			int division = 0;
			int residuo = 0;
			while (divisor >= dividendo)
			{
				divisor -= dividendo;
				residuo = divisor;
				division++;
			}
			//tiempo_ejecucion->Stop();
			//tiempo_division_residuo_iterativo += tiempo_ejecucion->ElapsedTicks;
			return "\nIteracion: Division = " + division + " Residuo = " + residuo;
		}
		
#pragma endregion

#pragma region Ejercicio 2
		int Mcd(int num1,int num2, int contador)
		{
			if (contador == 0)
			{
				if (num1 == 0 || num1 == 0)
					return 0;
			}

			if (num1 == 0)
				return num2;
			else
			{
				
				return Mcd((num2%num1), num1, contador+1);
			}
		}

		int McdIterativo(int num1, int num2) 
		{
			int pivot = 0;
			if (num1 == 0 || num1 == 0)
				return 0;

			while (num1>0)
			{	
				pivot = num1;
				num1 = num2 % num1;
				num2 = pivot;
			}

			return num2;
		}
#pragma endregion

#pragma region Ejercicio 3
	#pragma region Recursion

		int factorial(int n)
		{
			if (n <= 1)
				return 1;
			else
				return (n * factorial(n - 1));
		}
		//Funcion que realiza la combinatoria de dos numeros m y n;
		int coeficientePascal(int m, int n)
		{
			int resultado = factorial(m)/(factorial(n)*factorial(m-n));

			return resultado;
		}
		//Funcion que imprime los terminos del triangulo de pascal.
		String^ LineaTrianguloPascal(int m, int n, String^ resultado)
		{

			if (n == 0)
			{
				return resultado += 1;

			}
			else
			{
				int coeficiente = coeficientePascal(m, n);
				return resultado += coeficiente + " " + LineaTrianguloPascal(m, n - 1, resultado);

			}

		}
		void TrianguloPascal(int m, int n, String^ resultado)
		{
			if (n < 0)
			{
				Console::WriteLine("Fin Recursion");
			}
			else
			{
				lblTriangulo->Text += LineaTrianguloPascal(m, n, resultado)+"\n";
				TrianguloPascal(m - 1, n - 1, resultado);
			}
		}

	#pragma endregion 

	#pragma region Iteracion

		void TrianguloPascalIterativo(int m)
		{
			String^ resultado = "";
			for (int i = 0; i <=m; i++)
			{
				for (int j = 0; j <=i; j++)
				{
					resultado += coeficientePascal(i,j)+" ";
				}
				resultado += "\n";
			}

			lblTriangulo->Text = resultado;
		}

	#pragma endregion 
#pragma endregion 




	private: System::Void TextBox1_TextChanged(System::Object^ sender, System::EventArgs^ e) {
	}
	private: System::Void Label2_Click(System::Object^ sender, System::EventArgs^ e) {
	}

			 //BtnResultadoDivision.
	private: System::Void Button1_Click(System::Object^ sender, System::EventArgs^ e) {


		try
		{
			int divisor = Convert::ToInt32(txtNum1->Text);
			int dividendo = Convert::ToInt32(txtNum2->Text);

			if (divisor < dividendo)
				throw gcnew System::Exception;
			if (dividendo < 0 || divisor < 0)
				throw gcnew System::FormatException();

			lblResultadoDivision->Text = "Recurcion: Division = " + DivisionResta(divisor, dividendo) + " Residuo = " + ResiduoResta(divisor, dividendo) + DivisionResiduoIterativo(divisor, dividendo);
			
			/*MessageBox::Show("Tiempos de Ejecucion", "Tiempos:\nDivision Resta: "+tiempo_division_resta + "\nDivision Residuo: "+tiempo_division_residuo+"\nTiempo metodo iterativo: " + tiempo_division_residuo_iterativo, MessageBoxButtons(0), MessageBoxIcon(48));*/

			txtNum1->Clear();
			txtNum2->Clear();
			txtNum1->Focus();
		}
		catch (FormatException^)
		{
			MessageBox::Show("No ingreso un valor v�lido", "Error al ingresar datos", MessageBoxButtons(0), MessageBoxIcon(48));
			txtNum1->Clear();
			txtNum2->Clear();
			txtNum1->Focus();
		}
		catch (Exception^)
		{
			MessageBox::Show("El divisor debe ser mayor o igual que el dividendo", "Error al ingresar datos", MessageBoxButtons(0), MessageBoxIcon(48));
			txtNum1->Clear();
			txtNum2->Clear();
			txtNum1->Focus();
		}

	}

			 //BtnMcd
	private: System::Void BtnMcd_Click(System::Object^ sender, System::EventArgs^ e) {

		try
		{
			int numero1 = Convert::ToInt32(txtNum1->Text);
			int numero2 = Convert::ToInt32(txtNum2->Text);

			
			/*if (dividendo < 0 || divisor < 0)
				throw gcnew System::FormatException();*/

			lblResultadoDivision->Text = "El MCD de " + numero1 + " y " +numero2 + " es " + Mcd(numero1, numero2, 0) 
				+ "\nEl MCD Iterativo de " + numero1 + " y " + numero2 + " es " + McdIterativo(numero1, numero2);

			

			txtNum1->Clear();
			txtNum2->Clear();
			txtNum1->Focus();
		}
		catch (FormatException^)
		{
			MessageBox::Show("No ingreso un valor v�lido", "Error al ingresar datos", MessageBoxButtons(0), MessageBoxIcon(48));
			txtNum1->Clear();
			txtNum2->Clear();
			txtNum1->Focus();
		}
		


	}
private: System::Void BtnTriangulo_Click(System::Object^ sender, System::EventArgs^ e)
{
	try
	{
		int grado = Convert::ToInt16(txtGradoBinomio->Text);

		if (grado < 0)
		{
			MessageBox::Show("No ingrese un valor negativo", "Error al ingresar dato", MessageBoxButtons(0), MessageBoxIcon(48));
		}
			

		lblTriangulo->Text = "";

		TrianguloPascal(grado, grado, "");

	}
	catch (Exception^)
	{
		MessageBox::Show("Cuidado con el valor que ingresa", "Error al ingresar dato", MessageBoxButtons(0), MessageBoxIcon(48));
		txtGradoBinomio->Clear();
	}
	catch (FormatException^)
	{
		MessageBox::Show("No ingreso un valor v�lido", "Error al ingresar dato", MessageBoxButtons(0), MessageBoxIcon(48));
		txtGradoBinomio->Clear();

	}
}
private: System::Void BtnTrianguloIterativo_Click(System::Object^ sender, System::EventArgs^ e)
{
	try
	{
		int grado = Convert::ToInt16(txtGradoBinomio->Text);

		if (grado < 0)
		{
			MessageBox::Show("No ingrese un valor negativo", "Error al ingresar dato", MessageBoxButtons(0), MessageBoxIcon(48));
		}


		lblTriangulo->Text = "";

		TrianguloPascalIterativo(grado);

	}
	catch (Exception^)
	{
		MessageBox::Show("Cuidado con el valor que ingresa", "Error al ingresar dato", MessageBoxButtons(0), MessageBoxIcon(48));
		txtGradoBinomio->Clear();
	}
	catch (FormatException^)
	{
		MessageBox::Show("No ingreso un valor v�lido", "Error al ingresar dato", MessageBoxButtons(0), MessageBoxIcon(48));
		txtGradoBinomio->Clear();

	}
}
};
}
